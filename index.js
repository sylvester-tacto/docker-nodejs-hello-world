const express = require('express');
const app = express();

const fs = require('fs');
const constants = JSON.parse(fs.readFileSync('./config/constants.json')) || 3000;

app.get('/', (req, res) => {
    res.send('Hello World!!');
})

app.listen(constants.PORT, () => console.log(`Port ${constants.PORT} is now listening!`))